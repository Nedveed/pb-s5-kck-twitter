import gui.desktop.Gui;

// Initialize GUI and run
public class MainDesktop {

  public static void main(String[] args) {
    Gui gui = new Gui();
    gui.run();
  }
}
