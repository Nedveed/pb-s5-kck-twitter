package data;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.disposables.Disposables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.conf.Configuration;

import java.util.Optional;

public class RxTwitterStream {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxTwitterStream.class);

    private RxTwitterStream() {
    }

    static public Flowable<Status> get() {
        return get(null, null);
    }

    static public Flowable<Status> get(FilterQuery... filterQueries) {
        return get(null, filterQueries);
    }

    static public Flowable<Status> get(Configuration configuration, FilterQuery... filterQueries) {
        return Flowable.create(subscriber -> {
            final TwitterStream twitterStream = Optional
                    .ofNullable(configuration)
                    .map(TwitterStreamFactory::new)
                    .orElseGet(TwitterStreamFactory::new)
                    .getInstance();

            subscriber.setDisposable(Disposables.fromRunnable(() -> {
                twitterStream.cleanUp();
                LOGGER.info("Cleaned up twitter stream after disposing");
            }));

            twitterStream.addListener(getStatusAdapter(subscriber));
            if (filterQueries != null) {
                for (FilterQuery filterQuery : filterQueries) {
                    twitterStream.filter(filterQuery);
                }
                LOGGER.info("Created new instance of TwitterStream and started filtered stream");
            } else {
                twitterStream.sample();
                LOGGER.info("Created new instance of TwitterStream and started sample stream");
            }
        }, BackpressureStrategy.LATEST);
    }

    private static StatusAdapter getStatusAdapter(FlowableEmitter<Status> subscriber) {
        return new StatusAdapter() {
            @Override
            public void onStatus(Status status) {
                LOGGER.debug("onStatus: " + status.toString());
                subscriber.onNext(status);
            }

            @Override
            public void onException(Exception ex) {
                LOGGER.error("Received exception from Twitter Streaming API", ex);
                subscriber.onError(ex);
            }

            @Override
            public void onTrackLimitationNotice(int arg0) {
                LOGGER.debug("RxTwitterStream onTrackLimitationNotice: " + arg0);
            }
        };
    }
}
