package data;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import twitter4j.FilterQuery;
import twitter4j.Status;

import java.util.Arrays;

public class TwitterStatusRepositoryImpl implements domain.repository.TwitterStatusRepository {

    private static volatile TwitterStatusRepositoryImpl INSTANCE = null;

    private TwitterStatusRepositoryImpl() {
    }

    public static TwitterStatusRepositoryImpl getInstance() {
        TwitterStatusRepositoryImpl tmp = INSTANCE;
        if (tmp == null) {
            synchronized (TwitterStatusRepositoryImpl.class) {
                tmp = INSTANCE;
                if (tmp == null) {
                    tmp = new TwitterStatusRepositoryImpl();
                }
            }
        }
        return tmp;
    }

    @Override
    public Flowable<Status> observeHashtags(String... hashtags) {
        if (hashtags.length <= 0)
            throw new IllegalArgumentException("Hashtags can't be empty");

        String[] parsedHashtags = Arrays.stream(hashtags)
                .map(String::trim)
                .filter(hashtag -> !hashtag.isEmpty())
                .filter(hashtag -> !hashtag.contains(" ")) // hashtag should be char sequence without blank chars
                .distinct()
                .map(hashtag -> hashtag.startsWith("#") ? hashtag : "#" + hashtag)
                .distinct()
                .toArray(String[]::new);

        if (parsedHashtags.length <= 0)
            throw new IllegalArgumentException("No hashtags after filtering, nothing to observe");

        return observeKeySentences(parsedHashtags);
    }

    @Override
    public Flowable<Status> observeKeySentences(String... sentences) {
        if (sentences.length == 0)
            throw new IllegalArgumentException("Sentences can't be empty");

        String[] parsedSentences = Arrays.stream(sentences)
                .map(String::trim)
                .filter(hashtag -> !hashtag.isEmpty())
                .distinct()
                .toArray(String[]::new);

        if (parsedSentences.length <= 0)
            throw new IllegalArgumentException("No sentences after filtering blank strings, nothing to observe");

        FilterQuery filterQuery = new FilterQuery()

                .track(parsedSentences);


        return RxTwitterStream.get(filterQuery)
                .subscribeOn(Schedulers.io());
    }
}
