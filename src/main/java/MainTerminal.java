import gui.terminal.Display;
import gui.terminal.Gui;

// Initialize GUI and run
public class MainTerminal {

  public static void main(String[] args) {
    Display dis= new Display();
    dis.runMenu();

  }
}
