package domain.repository;

import io.reactivex.Flowable;
import twitter4j.Status;

public interface TwitterStatusRepository {
    Flowable<Status> observeHashtags(String... hashtags);

    Flowable<Status> observeKeySentences(String... sentences);
}
