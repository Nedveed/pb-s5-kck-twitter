package domain.util.image;

import domain.util.ascii.ImageToAscii;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import javax.imageio.ImageIO;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.resizers.configurations.ScalingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(ImageUtil.class);
  private static final double HEIGHT_NORMALIZATION_FACTOR_FOR_ASCII_IMAGE = 0.15d;

  static public Optional<BufferedImage> downloadPhoto(URL imageUrl, int maxWidth, int maxHeight) {
    return downloadPhoto(imageUrl)
        .map(downloadedImage -> {
          if (maxWidth <= 0 || maxHeight <= 0 || downloadedImage.getWidth() <= maxWidth) {
            LOGGER.debug("Returned original image due to [maxWidth:" + maxWidth + "; maxHeight: "
                + maxHeight + "]");
            return downloadedImage;
          }
          try {
            return scale(downloadedImage, maxHeight, maxWidth, true);
          } catch (IOException e) {
            LOGGER.error("Exception while creating thumbnail[" + imageUrl.toString() + "]", e);
          }
          return null;
        });
  }

  static public Optional<BufferedImage> downloadPhoto(URL imageUrl) {

    if (imageUrl == null) {
      throw new NullPointerException("URL of image can't be null");
    }
    try {
      return Optional.ofNullable(ImageIO.read(imageUrl));
    } catch (IOException e) {
      LOGGER.warn("Exception while downloading image from URL[" + imageUrl.toString() + "]", e);
    }
    return Optional.empty();
  }


  private static BufferedImage scale(BufferedImage image, int maxWidth, int maxHeight,
      boolean keepAspectRatio) throws IOException {
    if (maxWidth <= 0 || maxHeight <= 0) {
      throw new IllegalArgumentException("Dimentions of the image can't be <= 0"
          + "["
          + "; maxWidth:"
          + maxWidth
          + "; maxHeight:"
          + maxHeight
          + "]"
      );
    }
    return Thumbnails.of(image)
        .width(maxWidth)
        .height(maxHeight)
        .scalingMode(ScalingMode.PROGRESSIVE_BILINEAR)
        .keepAspectRatio(keepAspectRatio)
        .asBufferedImage();
  }

  private static BufferedImage asciiImageNormalization(BufferedImage image, int maxHeight,
      int maxWidth) throws IOException {
    double wScale = (maxWidth > 0d) ? (double) maxWidth / (double) image.getWidth() : 1.0d;
    double hScale = (maxHeight > 0d) ? (double) maxHeight / (double) image.getHeight() : 1.0d;
    hScale *= HEIGHT_NORMALIZATION_FACTOR_FOR_ASCII_IMAGE;
    return Thumbnails.of(image)
        .scale(wScale, hScale)
        .scalingMode(ScalingMode.PROGRESSIVE_BILINEAR)
        .asBufferedImage();
  }

  public static Optional<String> downloadPhotoAsAscii(URL imageUrl, int maxWidth, int maxHeight) {
    return downloadPhoto(imageUrl)
        .map(image -> silentAsciiImageNormalization(imageUrl, maxWidth, maxHeight, image))
        .map(image -> ImageToAscii.convert(image, false));
  }

  private static BufferedImage silentAsciiImageNormalization(URL imageUrl, int maxWidth,
      int maxHeight, BufferedImage image) {
    try {
      return asciiImageNormalization(image, maxHeight, maxWidth);
    } catch (IOException e) {
      String msg = "asciiImageNormalization using Thumbnails class ended up with error["
          + "imageUrl:"
          + imageUrl.toString()
          + "; maxWidth:"
          + maxWidth
          + "; maxHeight:"
          + maxHeight
          + "]";
      LOGGER.error(msg, e);
    }
    return null;
  }
}