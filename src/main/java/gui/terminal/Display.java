package gui.terminal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class Display {
    int width = 56;
    Scanner keyboard = new Scanner(System.in);

    Gui gui;

    public Display() {
        gui = new Gui();
        // Signal handler method
    }

    public void runMenu() {

        while (true) {
            printMenu();
            int choice = getMenuChoice();
            performAction(choice);

        }
    }


    private void printMenu() {
        displayHeader("Please make a selection");
        System.out.println("[1] Run");
        System.out.println("[2] Run with Your hashtag");
        System.out.println("[3] Run with Your sentence");
        System.out.println("[4] Authors");
        System.out.println("[0] Exit");
    }

    private int getMenuChoice() {
        Scanner keyboard = new Scanner(System.in);
        int choice = -1;
        do {
            System.out.print("Enter your choice: ");
            try {
                choice = Integer.parseInt(keyboard.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid selection. Numbers only please.");
            }
            if (choice < 0 || choice > 4) {
                System.out.println("Choice outside of range. Please chose again.");
            }
        } while (choice < 0 || choice > 4);
        return choice;
    }

    private void performAction(int choice) {
        switch (choice) {
            case 0:
                System.out.println("Thank you for using our application.");
                System.exit(0);
                break;
            case 1: {

                gui.run(0, "trump", "messi");
            }
            break;
            case 2:

                System.out.print("Enter your hashtag: ");
                String hash;
                hash = keyboard.nextLine();

                gui.run(1, hash);
                break;
            case 3:

                System.out.print("Enter your sentence: ");
                String sent;
                sent = keyboard.nextLine();

                gui.run(0, sent);
                break;
            case 4:
                displayHeader("Zuzanna Maciulewska  Patryk Gacki  Szymon Małyszko");

                break;
            default:
                System.out.println("Unknown error has occured.");
        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            gui.stop();
        }
    }

    public void runTweet(String name, String screen, String text) {


        displayTable(text, screen, name);
    }

    private String getHashtag() {

        String hash;
        System.out.print("Enter your hashtag: ");

        hash = keyboard.nextLine();
        return hash;
    }

    private String getTweet() {
        Scanner keyboard = new Scanner(System.in);
        String tweet;
        System.out.print("Enter your tweet: ");

        tweet = keyboard.nextLine();
        return tweet;
    }

    private void displayHeader(String message) {
        System.out.println();
        int width = message.length() + 6;
        StringBuilder sb = new StringBuilder();
        sb.append("+");
        for (int i = 0; i < width; ++i) {
            sb.append("-");
        }
        sb.append("+");
        System.out.println(sb.toString());
        System.out.println("|   " + message + "   |");
        System.out.println(sb.toString());
    }

    private void displayTweet(String message) {

        if (message.length() < 50) {

            StringBuilder sb = new StringBuilder();
            StringBuilder mes = new StringBuilder();
            sb.append("* ");
            sb.append("+");
            for (int i = 0; i < width - 2; ++i) {
                sb.append("-");

                if (i < message.length()) {
                    if (message.charAt(i) == '\n') mes.append(" ");
                    else mes.append(message.charAt(i));
                } else if (i < 50) mes.append(" ");
            }


            sb.append("+");
            sb.append(" *");
            System.out.println(sb.toString());
            System.out.println("* |  " + mes.toString() + "  | *");
            System.out.println(sb.toString());
        } else if (message.length() < 100) {

            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < message.length(); i++) {
                if (i < 50) {
                    if (message.charAt(i) == '\n') sb1.append(" ");
                    else sb1.append(message.charAt(i));
                } else if (message.charAt(i) == '\n') sb2.append(" ");
                else sb2.append(message.charAt(i));
            }
            while (sb2.toString().length() < 50) sb2.append(" ");

            StringBuilder sb = new StringBuilder();
            sb.append("* ");
            sb.append("+");
            for (int i = 0; i < width - 2; ++i) {
                sb.append("-");
            }
            sb.append("+");
            sb.append(" *");
            System.out.println(sb.toString());
            System.out.println("* |  " + sb1.toString() + "  | *");
            System.out.println("* |  " + sb2.toString() + "  | *");
            System.out.println(sb.toString());
        } else {
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb3 = new StringBuilder();
            for (int i = 0; i < message.length(); i++) {
                if (i < 50) {
                    if (message.charAt(i) == '\n') sb1.append(" ");
                    else sb1.append(message.charAt(i));
                } else if (i < 100) {
                    if (message.charAt(i) == '\n') sb2.append(" ");
                    else sb2.append(message.charAt(i));
                } else if (message.charAt(i) == '\n') sb3.append(" ");
                else sb3.append(message.charAt(i));
            }
            while (sb3.toString().length() < 50) sb3.append(" ");

            StringBuilder sb = new StringBuilder();

            sb.append("* +");
            for (int i = 0; i < width - 2; ++i) {
                sb.append("-");
            }
            sb.append("+ *");

            System.out.println(sb.toString());
            System.out.println("* |  " + sb1.toString() + "  | *");
            System.out.println("* |  " + sb2.toString() + "  | *");
            System.out.println("* |  " + sb3.toString() + "  | *");
            System.out.println(sb.toString());
        }
    }

    private void displayTable(String tweet, String screen, String name) {
        StringBuilder sb = new StringBuilder();

        sb.append("*");
        for (int i = 0; i < width + 2; ++i) {
            sb.append("*");
        }
        sb.append("*");

        System.out.println(sb.toString());
        displayUsername(name, screen);
        displayTweet(tweet);
        System.out.println(sb.toString());

    }

    private void displayUsername(String m, String s) {
        StringBuilder sb = new StringBuilder();
        sb.append("* ");
        sb.append("+");
        for (int i = 0; i < width - 2; ++i) {
            sb.append("-");
        }
        sb.append("+");
        sb.append(" *");
        m=" Name: "+m+"     Time: "+getTime();
        s=" ScreenName: "+s;
        System.out.println(sb.toString());
        StringBuilder mes = new StringBuilder();
        mes.append("* ");
        mes.append("|");
        for (int i = 0; i < width - 2; ++i) {

            if (i < m.length()) mes.append(m.charAt(i));
            else if (i < 56) mes.append(" ");
        }
        mes.append("|");
        mes.append(" *");
        StringBuilder mes1 = new StringBuilder();
        mes1.append("* ");
        mes1.append("|");
        for (int i = 0; i < width - 2; ++i) {

            if (i < s.length()) mes1.append(s.charAt(i));
            else if (i < 56) mes1.append(" ");
        }
        mes1.append("|");
        mes1.append(" *");

        System.out.println(mes.toString());
        System.out.println(mes1.toString());
        System.out.println(sb.toString());

    }
    public String getTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }
}
