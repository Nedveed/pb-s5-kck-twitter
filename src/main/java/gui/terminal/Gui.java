package gui.terminal;

import data.TwitterStatusRepositoryImpl;
import domain.util.image.ImageUtil;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.MediaEntity;
import twitter4j.Status;

import java.net.URL;
import java.util.Optional;

// MainTerminal gui.terminal.Gui class
public class Gui {

    private static final Logger LOGGER = LoggerFactory.getLogger(Gui.class);
    Disposable disposable;

    public void run(int a, String... hash) {

        // Usage of TweeterStatusRepository
        TwitterStatusRepositoryImpl repo = TwitterStatusRepositoryImpl.getInstance();
        Flowable<Status> stream;
        // get stream with observed hashtags
        if (a == 1) {
            stream = repo.observeHashtags(hash);
        } else stream = repo.observeKeySentences(hash);
        // observe hashstags
        disposable = stream.subscribe((Status status) -> {

            StringBuilder sb = new StringBuilder();
            // do something with status

            if (status.getText().length() < 140) {
                Display dis = new Display();
                dis.runTweet(status.getUser().getName(), status.getUser().getScreenName(), status.getText());
                System.out.println();
            }

            // get media
            MediaEntity[] mediaEntities = status.getMediaEntities();

            // look for photo
            for (MediaEntity media : mediaEntities) {
                if (media.getType().equals("photo")) {

                    int width = 60;
                    int height = 200;

                    String mediaURL = media.getMediaURL();
                    Optional<String> asciiPhoto = ImageUtil.downloadPhotoAsAscii(new URL(mediaURL), width, height);

                    // do something with asciiPhoto
                    asciiPhoto.ifPresent(sb::append);
                }
                if (status.getText().length() < 140) {
                    System.out.println(sb.toString());
                }
            }

        });

        // Cancel obserivng hashtag
    }

    public void stop() {

        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }

        }
    }
}
